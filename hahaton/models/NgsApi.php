<?php

namespace App;

class NgsApi
{
    const URL_OFFICE_FOR_RENT = 'https://api.n1.ru/api/v1/offers/';
    const DISTRICT_CENTER_ID = 1306097;


    private static $offersDefaultParams = [
        'limit' => 10,
        'offset' => 0,
        'sort' => '-order_date,-creation_date',
        'query' => [
            [
                'rubric' => 'commercial',
                'deal_type' => 'rent_out',
                'filter' => [
                    'city_id' => 89026,
                    'type' => 'universal,retail,separate_building'
                ],
                'status' => 'published',
            ]
        ]
    ];

    /**
     * @param string $district
     * @param array $price
     *      min => int
     *      max => int
     * @param array $totalArea
     *      min =>  int
     *      max => int
     * @return array
     */
    public static function getOfficesForRent($district, array $price = [], array $totalArea = [])
    {
        $params = self::$offersDefaultParams;

        if (isset($price['max']) && !empty($price['max'])) {
            $params['query'][0]['filter']['price_max'] = $price['max'];
        }
        if (isset($price['min']) && !empty($price['min'])) {
            $params['query'][0]['filter']['price_min'] = $price['min'];
        }
        if (isset($totalArea['max']) && !empty($totalArea['max'])) {
            $params['query'][0]['filter']['total_area_max'] = $totalArea['min'];
        }
        if (isset($totalArea['min']) && !empty($totalArea['min'])) {
            $params['query'][0]['filter']['total_area_min'] = $totalArea['min'];
        }

        $params['filter_or']['district'] = self::getDistrictIdByName($district);

        if (!$result = self::call($params)) {
            return [];
        }

        foreach ($result['result'] as $key => $house) {
            if (empty($house['params']['location'])) {
                unset($result['result'][$key]);
            }
        }

        return $result['result'];
    }

    /**
     * API call for getting district id by name
     * @param $name
     * @return int
     */
    private static function getDistrictIdByName($name)
    {
        return self::DISTRICT_CENTER_ID;
    }

    private static function call(array $params)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL_OFFICE_FOR_RENT . '?' . http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);

        // echo self::URL_OFFICE_FOR_RENT . '?' . http_build_query($params) . PHP_EOL;

        curl_close($ch);

        if (empty($output)) {
            return false;
        }

        $result = json_decode($output, true);
        if (empty($result)) {
            return false;
        }

        return $result;
    }
}