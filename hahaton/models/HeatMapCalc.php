<?php

class HeatMapCalc
{
    const COEF_X = 150000;
    const COEF_Y = 85000;

    public static function get(array $point, $radius, array $similar = [], array $branches = [], array $houses = [], array $calcByParams = [])
    {
        $companiesNew = [];
        $housesNew = [];

        if (!empty($similar)) {
            foreach ($similar as $company) {
                $companyObj = new Company(new Point($company['lat'], $company['lng']), rand(0, 10) / 10, 500);
                $companiesNew[] = $companyObj;
            }
        }

        if (!empty($branches)) {
            foreach ($branches as $company) {
                if (!isset($company['lat']) || !isset($company['lng'])) {
                    continue;
                }

                $companyObj = new Company(new Point($company['lat'], $company['lng']), 1, 500);
                $companiesNew[] = $companyObj;
            }
        }

//        echo "<pre>";

        if (!empty($houses)) {
            foreach ($houses as $house) {

//                if (!isset($house['params']['location']['lon']) || !isset($house['params']['location']['lat'])) {
                    $housesObj = new House(new Point($house['params']['location']['lat'], $house['params']['location']['lon']), $house['params']['price'], $house['params']['total_area']);
//                print_r($housesObj);

                    $housesNew[] = $housesObj;
//                }
            }
        }

//        echo "</pre>"; die;

        $ourCompany = new Company(new Point($point['lat'], $point['lng']), 1, 500);
        $searchPoint = new SearchPoint($ourCompany, $radius, ($radius / self::COEF_X), ($radius / self::COEF_Y));


        $model = new HeatMapCreater($searchPoint, $companiesNew, $housesNew);

        $byHumans = isset($calcByParams['people']) && $calcByParams['people'];
        $byCompanies = isset($calcByParams['companies']) && $calcByParams['companies'];
        $byHouses = isset($calcByParams['houses']) && $calcByParams['houses'];

        $heat_map = $model->getHeatMap($byHumans, $byCompanies, $byHouses);

        return $heat_map;
    }
}