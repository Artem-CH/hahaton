<?php

namespace App;


use function FastRoute\TestFixtures\empty_options_cached;

class DoubleGisApi
{
    const URL_API = 'http://catalog.api.2gis.ru/';
    const PATH_SEARCH = 'search';
    const PATH_PROFILE = 'profile';
    const PATH_BRANCHES = 'firmsByFilialId';

    private static $defaultParams = [
        'version' => 1.3,
        'key' => 'ruoedw9225',
    ];

    public static function getCompanyProfile($id, $sort = null, $hash = null)
    {
        $params = self::$defaultParams;

        if (isset($id) && !empty($id)) {
            $params['id'] = $id;
        }

        if (isset($sort) && !empty($sort)) {
            $params['sort'] = $sort;
        }

        if (isset($hash) && !empty($hash)) {
            $params['hash'] = $hash;
        }

        //$params['show_see_also'] = 1;

        if (!$result = self::call($params, self::PATH_PROFILE)) {
            return [];
        }

        return $result;
    }

    public static function getCompanyBranches($firmId)
    {
        $params = self::$defaultParams;

//        $params['fields'] = 'items.region_id,
//                            items.reviews,
//                            items.rubrics,
//                            items.name_ex,
//                            items.point,
//                            items.org,
//                            items.ads.options,
//                            request_type,
//                            search_attributes
//        ';

        if (isset($firmId) && !empty($firmId)) {
            $params['firmid'] = $firmId;
        }

        if (!$result = self::call($params, self::PATH_BRANCHES)) {
            return [];
        }

        return $result['result'];
    }

    private static function call(array $params, $path)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL_API . $path . '?' . http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);

        curl_close($ch);

        if (empty($output)) {
            return false;
        }

        $result = json_decode($output, true);
        if (empty($result)) {
            return false;
        }

        return $result;
    }

    public static function getSimilarInfo($params)
    {
        if (isset($params) && !empty($params)) {
            foreach ($params as &$param) {
                $param['raiting'] = rand(1, 5);
                $result = self::getCompanyProfile($param['id'], $param['hash']);
                $param['point']['lat'] = isset($result['lat']) ? $result['lat'] : '';
                $param['point']['lng'] = isset($result['lon']) ? $result['lon'] : '';

                $_SESSION['similar'] = $params;
            }
        }else {
            return '';
        }
    }
}

