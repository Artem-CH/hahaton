<?php

class Point {
	public $lat;
	public $lon;

	public function __construct($lat, $lon) {
		$this->lat = $lat;
		$this->lon = $lon;
	}
}

class Direction {
	public $dx;
	public $dy;

	public function __construct($dx, $dy) {
		$this->dx = $dx;
		$this->dy = $dy;
	}
}

class House {
	public $position;
	public $costPerMeter; // min - 0, max - 5

	public function __construct($position, $cost, $space) {
		$this->position = $position;
		$this->costPerMeter = $cost / $space;
	}
}

class Company {
	public $position;
	public $rating; // min - 0, max - 5
	public $avgPrice;

	public function __construct($position, $rating, $avgPrice) {
		$this->position = $position;
		$this->rating = $rating;
		$this->avgPrice = $avgPrice;
	}
}

class ValuePoint {
	public $lat;
	public $lon;
	public $value;

	public function __construct($lat, $lon, $value) {
		$this->lat = $lat;
		$this->lon = $lon;
		$this->value = $value;
	}
}

/**
 * @param Point $position
 * @param double $R
 * @param double $latR
 * @param double $lonR
 */
class SearchPoint {
	public $company;
	public $R;
	public $latR;
	public $lonR;

	function __construct($company, $R, $latR, $lonR) {
		$this->company = $company;
		$this->R = $R;
		$this->latR = $latR;
		$this->lonR = $lonR;
	}
}

class HeatMapCreater {
	public $searchPoint;
	public $companies;
	public $houses;

	public $Nx = 100;
	public $Ny = 100;

	const step = 10;

	function __construct($searchPoint, $companies, $houses) {
		$this->searchPoint = $searchPoint;
		$this->companies = $companies;
		$this->houses = $houses;

		$this->Nx = 200;//2 * $searchPoint->R / self::step;
		$this->Ny = 200;// * $searchPoint->R / self::step;
	}

	public function getHeatMap($humans, $companies, $houses) {

		$blurRadius = 3.0;
		$result = [];
		//void array
		for ($x = 0; $x < $this->Nx; $x++) {
			for ($y = 0; $y < $this->Ny; $y++) {
				$result[$x][$y] = 0.0;
			}
		}

//echo '<pre>';
		if ($humans) {
			$humanField = $this->createHumanField();
//			print_r($humanField);
			$humanField = $this->normalize($humanField);
			$bluredHumanField = $this->blur($humanField, $blurRadius);

			$result = $this->addHeatLayer($result, $bluredHumanField);
		}
//echo '<pre>';
		if ($companies) {
			$this->companies = $this->convertToPositions($this->companies);
			$impactField = $this->impactField();
//			print_r($impactField);
//			$impactField = $this->normalize($impactField);

			$result = $this->addHeatLayer($result, $impactField);
		}

		if ($houses) {
			$this->houses = $this->convertToPositions($this->houses);
			$housesField = $this->housesField();
			//print_r($housesField);
//			$housesField = $this->normalize($housesField);

			$result = $this->addHeatLayer($result, $housesField);
		}
//echo '</pre>';die();

		$result = $this->normalize($result);
		$result = $this->convertToOutput($result);

		return $result;
	}

	/**
	 * @param two-dimensional array $array - Array which need normalize
	 * @return two-dimensional array
	 */
	private function normalize($array) {
		$max = 0.001;

		for ($x = 0; $x < $this->Nx; $x++) {
			for ($y = 0; $y < $this->Ny; $y++) {
				if (isset($array[$x][$y])) {
					$max = max($array[$x][$y], $max);
				}
			}
		}
		for ($x = 0; $x < $this->Nx; $x++) {
			for ($y = 0; $y < $this->Ny; $y++) {
				if (isset($array[$x][$y])) {
					$array[$x][$y] /= $max;
				}
			}
		}

		return $array;
	}

	/**
	 * @param two-dimensional array $array
	 * @return two-dimensional array
	 */
	private function invert($array) {
		$result = [];
		for ($x = 0; $x < $this->Nx; $x++) {
			for ($y = 0; $y < $this->Ny; $y++) {
				$result[$x][$y] =  1 - $array[$x][$y];
			}
		}
		return $result;
	}

	/**
	 * @param two-dimensional array $firstArray
	 * @param two-dimensional array $secondArray
	 * @return two-dimensional array
	 */
	private function addHeatLayer($firstArray, $secondArray) {
		$result = [];
		for ($x = 0; $x < $this->Nx; $x++) {
			for ($y = 0; $y < $this->Ny; $y++) {
				$result[$x][$y] =  $firstArray[$x][$y] + $secondArray[$x][$y];
			}
		}

		return $result;
	}

	/**
	 * @param two-dimensional array $array
	 * @param double $R
	 * @param double $x0
	 * @param double $y0
	 * @return double
	 */
	private function average($array, $R, $x0, $y0) {
		$average = 0.0;
		$count = 0;

		$startX = $x0 - $R;
		if ($startX < 0 ) {
			$startX = 0;
		}

		$endX = $x0 + $R;
		if ($endX >= $this->Nx - 1 ) {
			$endX = $this->Nx - 1;
		}


		$startY = $y0 - $R;
		if ($startY < 0 ) {
			$startY = 0;
		}

		$endY = $y0 + $R;
		if ($endY >= $this->Ny - 1 ) {
			$endY = $this->Ny - 1;
		}


		for ($x = $startX; $x <= $endX; $x++) {
			for ($y = $startY; $y <= $endY; $y++) {
				$length = sqrt((pow(floatval($x - $x0), 2) + pow(floatval($y - $y0), 2)));
				if ($length < $R) {
					$average += $array[$x][$y];
					$count++;
				}
			}
		}

		return $average / $count;
	}

	/**
	 * @param two-dimensional array $array
	 * @param double $R
	 * @return two-dimensional array
	 */
	private function blur($array, $R) {
		$result = [];
		for ($x = 0; $x < $this->Nx; $x++) {
			for ($y = 0; $y < $this->Ny; $y++) {
				$result[$x][$y] = $this->average($array, $R, $x, $y);
			}
		}

		$result = $this->normalize($result);
		return $result;
	}

	private function impactField() {
		$priceWeight = 0.5;
		$ratingWeight = 0.5;
		$scale = 7.0;

		for ($x = 0; $x < $this->Nx; $x++) {
			for ($y = 0; $y < $this->Ny; $y++) {
				$impactResult[$x][$y] = 0;
			}
		}

//		echo "<pre>"; print_r($this->companies); echo "</pre>"; die;

		for ($i = 0; $i < count($this->companies); $i++) {
			$company = $this->companies[$i];
			$impactRating = $ratingWeight * $company->rating;
			$impactPrice = $priceWeight * abs($company->avgPrice - $this->searchPoint->company->avgPrice) / $this->searchPoint->company->avgPrice;
			$totalImpact = $impactPrice + $impactRating + 0.01;
			$dispersion =  2;//$totalImpact;

			for ($x = 0; $x < $this->Nx; $x++) {
				for ($y = 0; $y < $this->Ny; $y++) {
					$impactResult[$this->Nx - $x - 1][$this->Ny - $y - 1] += 1 / (2 * M_PI * $dispersion) * exp( - (pow($x - $company->position->lat, 2) + pow($y - $company->position->lon, 2)) / pow($scale, 2) / (2 * $dispersion));
				}
			}
		}

		$impactResult = $this->normalize($impactResult);
		return $impactResult;
	}

	private function housesField() {

		$scale = 7.0;

		$maxPrice = 0.0;
		for ($i = 0; $i < count($this->houses); $i++) {
			$house = $this->houses[$i];
			$maxPrice = max($maxPrice, $house->costPerMeter);
		}

		for ($x = 0; $x < $this->Nx; $x++) {
			for ($y = 0; $y < $this->Ny; $y++) {
				$impactResult[$x][$y] = 0;
			}
		}
//		echo "<pre>"; print_r($this->houses); echo "</pre>"; die;

		for ($i = 0; $i < count($this->houses); $i++) {
			$house = $this->houses[$i];
			$totalImpact = $house->costPerMeter / $maxPrice + 0.01;
			$dispersion = 2.0 / $totalImpact;

			for ($x = 0; $x < $this->Nx; $x++) {
				for ($y = 0; $y < $this->Ny; $y++) {
					$impactResult[$this->Nx - $x - 1][$this->Ny - $y - 1] += 1 / (2 * M_PI * $dispersion) * exp( - (pow($x - $house->position->lat, 2) + pow($y - $house->position->lon, 2)) / pow($scale, 2) / (2 * $dispersion));
				}
			}

		}
		$impactResult = $this->normalize($impactResult);
		return $impactResult;
	}

	private function createHumanField() {
		// Init with zero
		$result = [];
		// var_dump($this->Nx, $this->Ny);
		for ($x = 0; $x < $this->Nx; $x++) {
			for ($y = 0; $y < $this->Ny; $y++) {
				$result[$x][$y] = 0.0;
			}
		}

//		for ($x = 40; $x < 60; $x++) {
//			for ($y = 0; $y < $this->Ny; $y++) {
//				$humans = 20 ;//* (1.0 + sin(2 * M_PI * $this->$w1)) / 2.0;
//				$result[$x][$y] = $humans;
//			}
//		}

		for ($x = 0; $x < $this->Nx; $x++) {
			for ($y = 30; $y < 32; $y++) {
				$humans = 1 ;//* (1.0 + cos(2 * M_PI * $this->$w2)) / 2.0;

//				$result[$x][$y] = $humans;
				$result[$x][$y + 50] = $humans;
				$result[$x][$y + 100] = $humans;

			}
		}

		return $result;
	}

	private function convertToPositions($companies) {
		$originLat = $this->searchPoint->company->position->lat;
		$originLon = $this->searchPoint->company->position->lon;

		$latR = $this->searchPoint->latR;
		$lonR = $this->searchPoint->lonR;

		$R = $this->searchPoint->R;
		$result = [];
		$count = 0;
		for ($i = 0; $i < count($companies); $i++) {
			$comp = $companies[$i];

			$lat = $comp->position->lat;
			$lon = $comp->position->lon;

			if (abs($originLat - $lat) < $latR && abs($originLon - $lon) < $lonR) {
				$x = intval(($originLat - $lat) / $latR * 100 + 100); //* $R  + $R);
				$y = intval(($originLon - $lon) / $lonR * 100 + 100);// * $R + $R);
				$comp->position = new Point($x, $y);
				$result[$count] = $comp;

				$count++;
			}
		}
		return $result;
	}

	private function convertToOutput($field) {

		$R = $this->searchPoint->R;

		$originLat = $this->searchPoint->company->position->lat - $this->searchPoint->latR;
		$originLon = $this->searchPoint->company->position->lon - $this->searchPoint->lonR;

		$latStep = $this->searchPoint->latR / 100;
		$lonStep = $this->searchPoint->lonR / 100;

		$index = 0;
		$result = [];
		for ($x = 0; $x < $this->Nx; $x++) {
			for ($y = 0; $y < $this->Ny; $y++) {
				$lat = $originLat + $x * $latStep;
				$lon = $originLon + $y * $lonStep;

//				if ($field[$x][$y] > 0.1) {
					$result[$index] = new ValuePoint($lat, $lon, $field[$x][$y]);
					$index++;
//				}
//				else if ($field[$x][$y] > 0.5) {
//					$impact = intval(( 1 - $field[$x][$y]) * 10);
//					for ($i = 0; $y < $impact; $i++) {
//						$result[$index] = new ValuePoint($lat, $lon, $field[$x][$y]);
//						$index++;
//					}
//
//				}

			}
		}
		return $result;
	}
}


?> 