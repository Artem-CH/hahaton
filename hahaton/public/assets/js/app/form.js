(function($) {
    var App = {};
    App.Form = {};

    function getValue() {
        console.log($(".select2-search__field").val());
        return $(".select2-search__field").val();
    }

    App.companiesFromAutocomplete = [];
    App.selectedCompany;
    App.similarCompanies = [];

    App.Form.initForm = function() {
        noUiSlider.create($('#slider-age').get(0), {
            start: [0, 40],
            tooltips: true,
            connect: true,
            range: {
                'min': 0,
                'max': 100
            },
            step: 1
        });

        noUiSlider.create($('#slider-price').get(0), {
            start: [0, 5000],
            tooltips: true,
            connect: true,
            range: {
                'min': 0,
                'max': 5000
            },
            step: 100
        });
    };
    App.Form.initForm();

    $('#company_name').autocomplete({
        serviceUrl: 'http://catalog.api.2gis.ru/search/?version=1.3&key=ruoedw9225&project=1',
        paramName: 'what',
        dataType: 'json',
        minChars: 3,
        transformResult: function (data) {
            if (data.result) {
                App.companiesFromAutocomplete = data.result;
                return {
                    suggestions: data.result.map(function(item, index) {
                        return { data: index, value: item.name + ' (' + item.address + ')'}
                    })
                };
            }
        },
        onSelect: function(suggestion) {
            var newSelectedCompany = App.companiesFromAutocomplete[suggestion.data];
            if (!App.selectedCompany || App.selectedCompany.id != App.selectedCompany.id) {
                App.selectedCompany = newSelectedCompany;

                // field
                $('#company_rubric').val(App.selectedCompany.rubrics[0]);

                // hidden fields
                $('#company_id').val(App.selectedCompany.id);
                $('#company_firm_group_id').val(App.selectedCompany.firm_group.id);
                $('#company_hash').val(App.selectedCompany.hash);
                $('#company_lat').val(App.selectedCompany.lat);
                $('#company_lng').val(App.selectedCompany.lon);

                App.Form.getSimilarCompanies();
            }
        }
    });

    App.Form.getSimilarCompanies = function() {
        console.log(App.selectedCompany);

        $.ajax({
            url: 'http://catalog.api.2gis.ru/searchinrubric',
            data: {
                what: App.selectedCompany.rubrics[0],
                point:  App.selectedCompany.lon + ',' + App.selectedCompany.lat,
                radius: 2000,
                page: 1,
                pagesize: 50,
                version: '1.3',
                key: 'ruoedw9225'
            },
            dataType: 'json',
            success: function(data) {
                if (data.result) {
                    data.result.forEach(function(item) {
                        if (item.id != App.selectedCompany.id && item.rubrics[0] == App.selectedCompany.rubrics[0]) {
                            App.similarCompanies.push(item);
                        }
                    });

                    App.Form.renderSimilarCompanies();
                }
            },
            error: function(data) {

            }
        });
    };

    App.Form.renderSimilarCompanies = function() {
        console.log(App.similarCompanies);
        var list = $('#similar-companies-list');
        var showFirst = 3;

        App.similarCompanies.forEach(function(item, index) {
            list.append('<li class="list-group-item" ' + (index > showFirst ? 'style="display:none"' : '') + '>'
                + item.name + ' (' + item.address + ')'
                + '<div style="float:right">'
                    + '<input type="radio" name="company[similar][' + item.id + '][flag]" value="1" id="company_similar_' + item.id + '_1" checked/>'
                    + '<label class="custom-radio-label" for="company_similar_' + item.id + '_1">Учитывать</label>'
                    + '<input type="radio" name="company[similar][' + item.id + '][flag]" value="2" id="company_similar_' + item.id + '_2" />'
                    + '<label class="custom-radio-label" for="company_similar_' + item.id + '_2">Не учитывать</label>'
                    + '<input type="radio" name="company[similar][' + item.id + '][flag]" value="3" id="company_similar_' + item.id + '_3" />'
                    + '<label class="custom-radio-label" for="company_similar_' + item.id + '_3">Учесть особенно</label>'
                    + '<input type="hidden" name="company[similar][' + item.id + '][id]" value="' + item.id + '"/>'
                    + '<input type="hidden" name="company[similar][' + item.id + '][hash]" value="' + item.hash + '"/>'
                    + '<input type="hidden" name="company[similar][' + item.id + '][lat]" value="' + item.lat + '"/>'
                    + '<input type="hidden" name="company[similar][' + item.id + '][lng]" value="' + item.lon + '"/>'
                + '</div>'
                + '<div style="clear:both">'
                + '</li>');


            if (index == showFirst) {
                list.append('<a href="#" class="list-group-item" id="show_all_similar">Показать всех (' + (App.similarCompanies.length - showFirst) + ')</a>');
            }

            if (index == App.similarCompanies.length - 1) {
                list.append('<a href="#" class="list-group-item" id="hide_all_similar">Скрыть</a>');
            }
        });

        $('#show_all_similar').on('click', function() {
            event.preventDefault();
            $('#hide_all_similar').css('display', 'block');
            list.find('.list-group-item').not(':visible').slideDown(300);
            $(this).hide();
        });

        $('#hide_all_similar').on('click', function() {
            event.preventDefault();

            list.find('.list-group-item').slice(showFirst).slideUp(300);
            $(this).hide();
            $('#show_all_similar').css('display', 'block');
        });

        $('#rent .section-border-bottom').show();
        $('#similar-companies').show();
    };
})(jQuery);
