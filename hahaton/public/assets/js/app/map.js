(function ($) {
    window.App = {};
    App.Map = {};
    App.Form = {};

    var paramsForm = $('#params-form-container');
    var mainPointCoordinates = {
        lat: Number($('#lat').val()),
        lng: Number($('#lng').val())
    };

    App.Form.initForm = function() {
        noUiSlider.create($('#radius').get(0), {
            start: 1500,
            tooltips: true,
            step: 10,
            connect: [true, false],
            range: {
                'min': 100,
                'max': 2000
            }
        });
    };
    App.Form.initForm();

    App.Map.init = function () {
        console.log(mainPointCoordinates);
        map = new google.maps.Map(document.getElementById('map'), {
            center: mainPointCoordinates,
            zoom: 13,
            icon: 'assets/images/icons/green.png',
            mapTypeControl: false,
            styles: [
                {
                    "featureType": "administrative.country",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        },
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "administrative.locality",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "administrative.neighborhood",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "administrative.land_parcel",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural.landcover",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural.landcover",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural.landcover",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural.terrain",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural.terrain",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "poi.attraction",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "poi.business",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "poi.government",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "poi.medical",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#a5d6a7"


                        }
                    ]
                },
                {
                    "featureType": "poi.place_of_worship",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "poi.place_of_worship",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "poi.school",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "lightness": "-2"
                        }
                    ]
                },
                {
                    "featureType": "poi.sports_complex",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "lightness": "-3"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "hue": "#ffb300"
                        },
                        {
                            "lightness": "-19"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.highway.controlled_access",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#c8c8c8"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#ccc5c5"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#141313"
                        }
                    ]
                },
                {
                    "featureType": "transit.line",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#656557"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.bus",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.rail",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "hue": "#ff6f00"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.rail",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "hue": "#0079ff"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#90caf9"
                        }
                    ]
                }
            ]
        });

        App.Map.addCenterMarker();
        App.Map.fillSelectedArea();
        App.Map.addParamsFormButton();

        App.Map.renderHeatmap(App.Map.getHeatMapPoints());

        App.Form.addRadiusChangeListener();
        App.Map.initHousesForRent();
        App.Map.initSimilar();



        $('#close-params-form').on('click', App.Map.closeParamsForm);
        $('#params-form').on('submit', App.Map.recalculate)
    };

    App.Map.addParamsFormButton = function () {
        function CenterControl(controlDiv, map) {
            // Set CSS for the control border.
            var controlUI = document.createElement('div');
            controlUI.id = 'open-params-form';
            controlUI.title = 'Click to recenter the map';
            controlDiv.appendChild(controlUI);

            // Setup the click event listeners: simply set the map to Chicago.
            controlUI.addEventListener('click', App.Map.openParamsForm);
        }

        var centerControlDiv = document.createElement('div');
        var centerControl = new CenterControl(centerControlDiv, map);

        centerControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.LEFT_CENTER].push(centerControlDiv);
    };

    App.Map.openParamsForm = function (event) {
        event.preventDefault();

        paramsForm.css({
            'width': paramsForm.data('width') + 'px',
            'visibility': 'visible'
        });
        $('#open-params-form').hide();
    };

    App.Map.closeParamsForm = function (event) {
        event.preventDefault();

        paramsForm.css('width', 0);
        setTimeout(function () {
            paramsForm.css('visibility', 'hidden');
            $('#open-params-form').show();
        }, 500);
    };

    App.Map.addCenterMarker = function () {
        App.Map.centerPoint = new google.maps.Marker({
            position: mainPointCoordinates,
            map: map,
            draggable: true,
        });
        App.Map.centerPoint.addListener('drag', function (event) {
            App.Map.selectedArea.setCenter(App.Map.centerPoint.getPosition());
        });
    };

    App.Map.fillSelectedArea = function () {
        App.Map.selectedArea = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.30,
            map: map,
            center: mainPointCoordinates,
            radius: 1500
        });
    };

    App.Form.addRadiusChangeListener = function () {
        $('#radius').get(0).noUiSlider.on('slide', function (event) {
            App.Map.selectedArea.setRadius(Number($('#radius').get(0).noUiSlider.get()));
            console.log($('#radius').get(0).noUiSlider.get() + 1);
        });
    };

    App.Map.recalculate = function (event) {
        var point = App.Map.centerPoint.getPosition();
        console.log(point);
        event.preventDefault();
        $.ajax({
            url: '/recalculate',
            type: 'POST',
            dataType: 'json',
            data: {
                radius: $('#radius').get(0).noUiSlider.get(),
                rentMin: $('#rentMin').val(),
                rentMax: $('#rentMax').val(),
                areaMin: $('#areaMin').val(),
                areaMax: $('#areaMax').val(),
                calc_by_branches: $('#calc_by_branches').is(':checked'),
                calc_by_people: $('#calc_by_people').is(':checked'),
                calc_by_house: $('#calc_by_house').is(':checked'),
                point: {
                    lat: point.lat(),
                    lng: point.lng()
                }
            },
            success: function(data) {
                if (data) {
                    var heatmapData = [];
                    data.forEach(function(item) {
                        heatmapData.push({
                            weight: item.value,
                            location: new google.maps.LatLng(Number(item.lat), Number(item.lon)),
                        });
                    });
                    console.log(heatmapData);
                    App.Map.renderHeatmap(heatmapData);
                }
            },
            error: function() {
            }
        });
    };

    App.Map.initHousesForRent = function() {
        var pinColor = "FE7569";
        var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|0000FF",
            new google.maps.Size(21, 34),
            new google.maps.Point(0,0),
            new google.maps.Point(10, 34));
        var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
            new google.maps.Size(40, 37),
            new google.maps.Point(0, 0),
            new google.maps.Point(12, 35));

        $('.js-house-for-rent').each(function(i, el) {
            var point = new google.maps.Marker({
                position: {
                    lat: Number($(el).data('lat')),
                    lng: Number($(el).data('lng'))
                },
                map: map,
                draggable: false,
                icon: 'assets/images/icons/blue.png',
                shadow: pinShadow
            });
            //
            // google.maps.event.addListener(point, 'click', function() {
            //     point.setVisible(false);
            //     alert($('#calc_by_house').val());
            // });

            point.addListener('click', function(event) {
                $.ajax({
                    url: '/houseForRent/' + $(el).val(),
                    dataType: 'html',
                    success: function(data) {
                        console.log(event);
                        $('body').append(
                            '<dov id="popup_overlay"><div id="map_popup_container">' + data + '</div></div>'
                        );
                        $('#map_popup_container').show(300);
                        $('#map_popup_container').click(function(event){
                            event.stopPropogation();
                            return false;
                        });
                        $('#popup_overlay').click(function(event) {
                            $(this).hide(300, function() {
                                $(this).remove();
                            });
                        });
                    },
                    error: function () {
                    }
                });
            });
        });
    };

    App.Map.initSimilar = function() {
        var pinColor = "ffb600";
        var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|ffb600",
            new google.maps.Size(21, 34),
            new google.maps.Point(0,0),
            new google.maps.Point(10, 34));
        var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
            new google.maps.Size(40, 37),
            new google.maps.Point(0, 0),
            new google.maps.Point(12, 35));

        $('.js-similar-info').each(function(i, el) {
            var point = new google.maps.Marker({
                position: {
                    lat: Number($(el).data('lat')),
                    lng: Number($(el).data('lng'))
                },
                map: map,
                draggable: false,
                icon: 'assets/images/icons/yellow.png',
                shadow: pinShadow
            });

            // point.addListener('click', function(event) {
            //     $.ajax({
            //         url: '/houseForRent/' + $(el).val(),
            //         dataType: 'html',
            //         success: function(data) {
            //             console.log(event);
            //             $('body').append(
            //                 '<dov id="popup_overlay"><div id="map_popup_container">' + data + '</div></div>'
            //             );
            //             $('#map_popup_container').show(300);
            //             $('#map_popup_container').click(function(event){
            //                 event.stopPropogation();
            //                 return false;
            //             });
            //             $('#popup_overlay').click(function(event) {
            //                 $(this).hide(300, function() {
            //                     $(this).remove();
            //                 });
            //             });
            //         },
            //         error: function () {
            //         }
            //     });
            // });
        });
    };

    App.Map.renderHeatmap = function(heatmapData) {
        if (App.Map.heatmap) {
            App.Map.heatmap.setMap(null);
        } else {
            App.Map.heatmap = new google.maps.visualization.HeatmapLayer({
                data: heatmapData
            });
            App.Map.heatmap.setMap(map);
        }
    };

    App.Map.getHeatMapPoints = function() {
        var coordinates = [];
        $('.js-heatmap-point').each(function(i, el) {
            coordinates.push({
                weight: $(el).val(),
                location: new google.maps.LatLng(Number($(el).data('lat')),(Number($(el).data('lng')))),
            });
        });
        console.log(coordinates);
        return coordinates;
    }
})(jQuery);