<?php

$container = $app->getContainer();

$container['\App\FormController'] = function($c) {
    $view = $c->get('view'); // retrieve the 'view' from the container
    return new \App\FormController($view);
};

$container['\App\MapController'] = function($c) {
    $view = $c->get('view'); // retrieve the 'view' from the container
    return new \App\MapController($view);
};

$container['\App\AjaxController'] = function($c) {
    $view = $c->get('view'); // retrieve the 'view' from the container
    return new \App\AjaxController($view);
};