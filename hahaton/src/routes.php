<?php
// Routes

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) use ($app) {

});

$app->get('/form', '\App\FormController:renderForm')->setName('form');
$app->post('/formSubmit', '\App\FormController:handleForm');

$app->get('/map', '\App\MapController:renderMap');

$app->get('/houseForRent/{id}', '\App\AjaxController:getHouseForRent');

$app->get('/houseForRent', '\App\AjaxController::getHouseForRent');
$app->post('/recalculate', '\App\AjaxController:recalculateMap');

$app->get('/getOffices', '\App\FormController:officesApi');
