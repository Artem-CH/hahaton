<?php

namespace App;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class MapController
{
    const NSK_CENTER_LAT = 55.03009331;
    const NSK_CENTER_LNG =  82.92050624;
    const NSK_CENTER_RADIUS = 1500;

    protected $container;
    protected $view;

    public function __construct(Twig $view)
    {
        $this->view = $view;
    }

    public function renderMap(Request $request, Response $response)
    {
        $coordinates = \HeatMapCalc::get(
            $_SESSION['point'],
            2000,
            $_SESSION['similar'],
            $_SESSION['branches'],
            $_SESSION['houses_for_rent'],
            [
                'people' => true,
                'companies' => false,
                'houses' => true,
            ]
        );

        return $this->view->render($response, 'map.html', [
            'maps_api_key' => 'AIzaSyAWl7LAtAziBIZuDuF6W8E277CFgAB0fxI',
            'params' => [
                'point' => [
                    'lat' => $_SESSION['point']['lat'],
                    'lng' => $_SESSION['point']['lng'],
                ],
                'radius' => self::NSK_CENTER_RADIUS
            ],
            'rent' => [
                'min' => $_SESSION['rent']['min'],
                'max' => $_SESSION['rent']['max'],
            ],
            'area' => [
                'min' => $_SESSION['area']['min'],
                'max' => $_SESSION['area']['max'],
            ],
            'houses_for_rent' => $_SESSION['houses_for_rent'],
            'similar_info' => $_SESSION['similar'],
            'heatmap' => $coordinates
        ]);
    }
}