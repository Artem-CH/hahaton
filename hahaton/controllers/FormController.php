<?php

namespace App;

use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use App\NgsApi;
use App\DoubleGisApi;

class FormController
{
    protected $container;
    protected $view;

    public function __construct(Twig $view)
    {
        $this->view = $view;
    }

    public function renderForm(Request $request, Response $response)
    {
        return $this->view->render($response, 'form.html');
    }

    public function officesApi(Request $request, Response $response)
    {
        echo '<pre>';
        print_r($_SESSION['houses_for_rent']);
        echo '</pre>';
    }

    public function handleForm(Request $request, Response $response)
    {
        $params = $request->getParams();

        $_SESSION['id'] = isset($params['company']['id']) ? $params['company']['id'] : '';
        $_SESSION['hash'] = isset($params['company']['hash']) ? $params['company']['hash'] : '';
        $_SESSION['firm_group_id'] = isset($params['company']['firm_group_id']) ? $params['company']['firm_group_id'] : '';
        $_SESSION['point']['lat'] = isset($params['company']['lat']) ? $params['company']['lat'] : '';
        $_SESSION['point']['lng'] = isset($params['company']['lng']) ? $params['company']['lng'] : '';

        DoubleGisApi::getSimilarInfo($params['company']['similar']);

        $_SESSION['people'] = isset($params['people']) ? $params['people'] : '';
        $_SESSION['rent'] = isset($params['rent']['price']) ? $params['rent']['price'] : '';
        $_SESSION['area'] = isset($params['rent']['area']) ? $params['rent']['area'] : '';
        $_SESSION['branches'] = DoubleGisApi::getCompanyBranches($_SESSION['firm_group_id']);
        $_SESSION['houses_for_rent'] = NgsApi::getOfficesForRent('', $_SESSION['rent'], $_SESSION['area']);

        return $response->withRedirect('/map');
    }
}