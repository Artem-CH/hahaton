<?php

namespace App;

use HeatMapCalc;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

class AjaxController
{
    protected $view;

    public function __construct(Twig $view)
    {
        $this->view = $view;
    }

    public function getHouseForRent(Request $request, Response $response, $args)
    {
        return $this->view->render($response, 'ajax/house-card.html', [
            'house' => $_SESSION['houses_for_rent'][$args['id']]
        ]);
    }

    public function recalculateMap(Request $request, Response $response)
    {
        $params = $request->getParams();

        $_SESSION['rent']['min'] = isset($params['rentMin']) ? $params['rentMin'] : '';
        $_SESSION['rent']['max'] = isset($params['rentMax']) ? $params['rentMax'] : '';
        $_SESSION['area']['min'] = isset($params['areaMin']) ? $params['areaMin'] : '';
        $_SESSION['area']['min'] = isset($params['areaMax']) ? $params['areaMax'] : '';

        $_SESSION['radius'] = isset($params['radius']) ? $params['radius'] : '';
        $_SESSION['point'] = $params['point'];

        $_SESSION['houses_for_rent'] = NgsApi::getOfficesForRent('', $_SESSION['rent'], $_SESSION['area']);

        $_SESSION['calc_by']['people'] = (boolean)$params['calc_by_people'];
        $_SESSION['calc_by']['companies'] = (boolean)$params['calc_by_branches'];
        $_SESSION['calc_by']['houses'] = (boolean)$params['calc_by_house'];

        $coordinates = HeatMapCalc::get(
            $_SESSION['point'],
            $_SESSION['radius'],
            $_SESSION['similar'],
            $_SESSION['branches'],
            $_SESSION['houses_for_rent'],
            [
                $_SESSION['calc_by']['people'],
                $_SESSION['calc_by']['companies'],
                $_SESSION['calc_by']['houses']
            ]
        );

        echo json_encode($coordinates);
        die();
    }
}